#!/usr/bin/env python
# Start server and database

import os
import cherrypy
import pymongo
import subprocess

from app.giuseppe import Giuseppe
from settings import ROOT_PATH

conf = {
  'tools.staticdir.on': True,
  'tools.staticdir.dir': ROOT_PATH + "/static",
  'tools.staticdir.index': 'index.html',
  
  'tools.sessions.on': True,
  'tools.sessions.timeout': 60,
  
  'tools.caching.on': False,
}

# Create a connection and store it in the current thread 
def connect(thread_index):
    cherrypy.thread_data.db = pymongo.Connection().giuseppe
 
# Tell CherryPy to call "connect" for each thread, when it starts up 
cherrypy.engine.subscribe('start_thread', connect)

# Start MongoDB
subprocess.Popen(["mongod"])

cherrypy.config.update(conf)
cherrypy.quickstart(Giuseppe())
