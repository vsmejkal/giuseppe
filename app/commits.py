#!/usr/bin/env python

import os
import git
import threading
import cherrypy
import pymongo
import time

from app.utils import c_date, day_begin


class Commits:
  @staticmethod
  def daily(repo, since, until):
    """Number of commits per each day with record."""
    day = 24 * 60 * 60
    next_day = since + day
    data = []
    count = 0
    
    for c in repo.iter_commits(since=since, until=until, reverse=True):
      date = c_date(c)
      c_day = day_begin(date)
      
      if c_day >= next_day:
        next_day = c_day + day
        if count > 0:
          data.append((int(c_day - day), count))
          count = 0
          
      count += 1

    return data
    
  @staticmethod
  def weekday(repo, since, until):
    """Number of commits per day of week."""
    dow_count = [0] * 7
    
    for c in repo.iter_commits(since=since, until=until, reverse=True):
      weekday = c_date(c).weekday()
      dow_count[weekday] += 1
      
    return dow_count

  @staticmethod
  def hourly(repo, since, until):
    """Number of commits per hour of day."""
    hours = [0] * 24
    
    for c in repo.iter_commits(since=since, until=until, reverse=True):
      h = c_date(c).hour
      hours[h] += 1
      
    return hours
    
  @staticmethod
  def monthly(repo, since, until):
    """Number of commits per month of year."""
    months = [0] * 12
    
    for c in repo.iter_commits(since=since, until=until, reverse=True):
      m = c_date(c).month
      months[m - 1] += 1
      
    return months
    
