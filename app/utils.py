#!/usr/bin/env python

import json
import cherrypy
import time
from datetime import datetime


def output(state="OK", data=None):
  """Return the JSON with state and serialized object"""
  cherrypy.response.headers['Content-Type']= 'application/json'
  
  return json.dumps({
    "status": state,
    "data": data
  }, default=str)
  

def c_date(c):
  """Return datetime of commit with respect to timezone"""
  tstamp = c.committed_date         # author time
  tstamp -= c.committer_tz_offset   # GMT time
  tstamp += time.timezone           # local time
  return datetime.fromtimestamp(tstamp)
  
def day_begin(date):
  """Translate some datetime to begin of day"""
  ts = time.mktime(date.timetuple())
  ts -= date.hour * 60 * 60 + date.minute * 60 + date.second
  return ts