#!/usr/bin/env python


class Developers:
  @staticmethod
  def list(repo, since, until):
    """Summary of developers and their commits"""
    developers = {}
    
    for c in repo.iter_commits(since=since, until=until):
      stats = c.stats.total
      name = c.committer.name
      
      if not name in developers:
        developers[name] = {
          "commits": 0,
          "ins": 0,
          "del": 0
        }
      
      developers[name]["commits"] += 1
      developers[name]["ins"] += stats["insertions"]
      developers[name]["del"] += stats["deletions"]
    
    # Sort by number of commits
    names = developers.keys()
    names.sort(key=lambda x: developers[x]["commits"], reverse=True)
    developers = map(lambda n: (n, developers[n]), names)
    
    return developers
