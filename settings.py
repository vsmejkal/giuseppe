#!/usr/bin/env python

import os

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
REPOS_PATH = ROOT_PATH + "/repos"
STATIC_PATH = ROOT_PATH + "/static"