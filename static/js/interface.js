/**
 * User Interface Initialization
 */
 
var gsp = new Giuseppe();
gsp.initialize();
 
 
// ============================================================================

// Initialize empty anchors
$("a[href=]").attr("href", "javascript:null");

// Initialize shorten tool
$.fn.shorten.defaults = {
  tail: "&hellip;",
  tooltip: true
};

// ============================================================================

// Add new GIT project dialog
$("#win_add_project, #win_clone_progress").modal({backdrop: true});

$("#btn_add_project").bind("click", function() {
  $("#win_add_project").modal("show");
});

$("#btn_manage_projects").bind("click", function(e) {
  gsp.loadPage("projects");
});

$("#win_add_project_cancel").bind("click", function() {
  $("#win_add_project").modal('hide');
});

$("#win_add_project_import").bind("click", function() {
  var name = $("#input-project-name").val();
  var url = $("#input-project-url").val();
  
  if (!name)
    gsp.errorMessage("Name of project is missing!");
  else if (!url)
    gsp.errorMessage("Remote repository URL is missing!");
  else {
    gsp.startCloning(name, url);
    $("#input-project-name").val("");
    $("#input-project-url").val("");
    $("#win_add_project").modal('hide');
    $("#win_clone_progress").modal('show');
  }
});

// ============================================================================

// Clone progress dialog
$("#win_clone_progress_btn_ok").bind("click", function() {
  $("#win_clone_progress").modal("hide");
  gsp.reloadPage();
});

// ============================================================================

// Content switcher
$("#mainmenu a, a.brand").bind("click", function () {
  location.href = this.href;
  gsp.reloadPage();
  return false;
});

// Switch project
$("#projects_list a.project").live("click", function() {
  var name = $(this).text();
  gsp.selectProject(name);
});

