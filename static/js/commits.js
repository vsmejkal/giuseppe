/** 
 * Commits JS handler
 */
 
function Commits()
{
  this.initialize = function() {
    this.initDatePickers();
    this.reloadCharts();
  };
  
  // Set up date choosers
  this.initDatePickers = function() {
    var commits = this;
    var month = 30 * 24 * 60 * 60;
    var since = gsp.date((new Date()).getTime() / 1000 - month);
    var until = gsp.date((new Date()).getTime() / 1000);
    
    $("#date_from")
      .datepicker()
      .val(since)
      .bind("change", function(){ commits.reloadCharts.call(commits); });
      
    $("#date_to")
      .datepicker()
      .val(until)
      .bind("change", function(){ commits.reloadCharts.call(commits); });
  };
  
  // Fetch data from server and reload charts
  this.reloadCharts = function() {
    var project = gsp.getSelectedProject();
    var since = gsp.date($("#date_from").val());
    var until = gsp.date($("#date_to").val());
    var params = {"name": project, "since": since, "until": until};

    gsp.request("commits", $.extend(params, {"group": "daily"}), this.dailyChart);
    gsp.request("commits", $.extend(params, {"group": "weekday"}), this.dayInWeekChart);
    gsp.request("commits", $.extend(params, {"group": "hourly"}), this.hourOfDayChart);
    gsp.request("commits", $.extend(params, {"group": "monthly"}), this.monthlyChart);
  };
  
  // Chart with all commits in groups
  this.dailyChart = function(data) {
    for (var i in data)
      data[i][0] *= 1000;
    
    chart = new Highcharts.StockChart({
      credits:{
        enabled: false
      },
      chart: {
        renderTo: 'daily_commits',
        alignTicks: false,
        borderColor: "#e9e9e9",
        borderWidth: 1
      },
      rangeSelector: {
        enabled: false
      },
      title: {
          text: ''
      },
      series: [{
        type: 'column',
        name: 'Commits',
        data: data,
        dataGrouping: {
          units: [[
            'week', // unit name
            [1] // allowed multiples
          ], [
            'month',
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
          ]]
        }
      }]
    });
  };
  
  // Chart of commits averaged on days of week
  this.dayInWeekChart = function(data) {
    var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    
    for (var i in data)
      data[i] = [days[i], data[i]];
    
    chart = new Highcharts.Chart({
      series: [
        {
          "name":"Days in week",
          "type":"pie",
          "sliced":true,
          "data": data
        }
      ],
      tooltip: {
         formatter: function() {
            return '<b>'+ Math.round(this.point.percentage) +' %</b>';
         }
      },
      "title":{
        "text":null
      },
      "legend":{
        "enabled": false
      },
      "chart":{
        renderTo:"dayinweek_commits",
        borderWidth: 1,
        borderColor: "#e9e9e9",
        plotShadow: false,
        plotBackgroundColor: null,
        plotBorderWidth: null,
      },
      
      credits: {
        enabled: false
      }
    });
  };
  
  // Chart of commits averaged on hours of day
  this.hourOfDayChart = function(data) {
    chart3 = new Highcharts.Chart({
      credits:{
        "enabled":false
      },
      chart: {
         renderTo: 'hourinday_commits',
         defaultSeriesType: 'line',
         borderWidth: 1,
        borderColor: "#e9e9e9",
      },
      title: {
         text: ''
      },
      xAxis: {
         categories: [0]
      },
      yAxis: {
         title: {
            text: '# commits'
         },
         min: 0
      },
      legend: {
        enabled: false
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.x +':00 - '+ (this.x + 1) +':00</b><br/>'+
               this.y +' commits';
         }
      },
      plotOptions: {
         line: {
            dataLabels: {
               enabled: true
            }
         }
      },
      series: [{
         name: '',
         data: data
      }]
    });
  };
  
  // Chart of commits averaged on months
  this.monthlyChart = function(data) {
    chart = new Highcharts.Chart({
      credits: {
        enabled: false
      },
      chart: {
         renderTo: 'monthly_commits',
         defaultSeriesType: 'column',
         borderWidth: 1,
        borderColor: "#e9e9e9"
      },
      title: {
         text: ''
      },
      xAxis: {
         categories: [
            'Jan', 
            'Feb', 
            'Mar', 
            'Apr', 
            'May', 
            'Jun', 
            'Jul', 
            'Aug', 
            'Sep', 
            'Oct', 
            'Nov', 
            'Dec'
         ]
      },
      yAxis: {
         min: 0,
         title: {
            text: '# commits'
         }
      },
      legend: {
        enabled: false
      },
      tooltip: {
         formatter: function() {
            return '<b>'+ this.x +'</b>: '+ this.y +' commits';
         }
      },
      plotOptions: {
         column: {
            pointPadding: 0.2,
            borderWidth: 0
         }
      },
      series: [
       {
         name: 'Commits',
         data: data
   
      }]
    });
  };
};

var loader = function(){ (new Commits()).initialize(); };

   