/** 
 * Developers JS handler
 */
 
function Developers()
{
  this.initialize = function() {
    this.initDatePickers();
    this.reloadTable();
  };
  
  // Set up date choosers
  this.initDatePickers = function() {
    var devs = this;
    var month = 30 * 24 * 60 * 60;
    var since = gsp.date((new Date()).getTime() / 1000 - month);
    var until = gsp.date((new Date()).getTime() / 1000);
    
    $("#date_from")
      .datepicker()
      .val(since)
      .bind("change", function(){ devs.reloadTable.call(devs); });
      
    $("#date_to")
      .datepicker()
      .val(until)
      .bind("change", function(){ devs.reloadTable.call(devs); });
  };
  
  // Fill in list of developers
  this.reloadTable = function() {
    var project = gsp.getSelectedProject(),
        since = gsp.date($("#date_from").val()),
        until = gsp.date($("#date_to").val()),
        params = {"name": project, "since": since, "until": until};
    
    gsp.request("developers", params, function(dt) {
      var rows = [];
      
      for (var i in dt) {
        var name = dt[i][0],
            stats = dt[i][1],
            s = '<tr>';
            
        s += '<td>'+ (parseInt(i) + 1) +'</td><td>'+ name +'</td>';
        s += '<td>'+ stats.commits +'</td><td>'+ stats.ins +'</td><td>'+ stats.del +'</td>';
        s += '</tr>';
        rows.push(s);
      }
      
      // Add rows to table
      $("#developers_table").html(rows.join('\n'));
    });
  };
}


var loader = function(){ (new Developers()).initialize(); };
