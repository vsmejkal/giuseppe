/** 
 * Summary JS handler
 */
 
function Summary()
{
  this.initialize = function() {
    var summary = this;
    
    // Range switcher
    $("#summary_range a").bind("click", function() {
      $(this).parent().find(".active").removeClass("active");
      $(this).addClass("active");
      summary.reload();
      return false;
    });
    
    summary.reload();
  };
  
  // Get time range for summary
  this.getScope = function() {
    var scopes = ["7days", "30days", "100commits"];
    return scopes[$("#summary_range a.active").index()];
  };
  
  // Reload all data
  this.reload = function() {
    var summary = this;
    var scope = this.getScope();
    var project = gsp.getSelectedProject();
    
    // Load commits
    gsp.request("summary", {
      "name": project,
      "scope": scope,
      "section": "commits"
    }, summary.renderCommits);
    
    // Load developers
    gsp.request("summary", {
      "name": project,
      "scope": scope,
      "section": "developers"
    }, summary.renderDevelopers);
    
    // Load files
    gsp.request("summary", {
      "name": project,
      "scope": scope,
      "section": "files"
    }, summary.renderFiles);
  };
  
  // Make insertions and deletions pie chart and table
  this.renderCommits = function(dt) {
    $("#commits_total").html(dt.count);
    $("#commits_insertions").html(dt.ins);
    $("#commits_deletions").html(dt.del);
    
    var chart = new Highcharts.Chart({
      chart: {
         renderTo: 'commits_chart',
         plotBackgroundColor: null,
         plotBorderWidth: null,
         plotShadow: false,
         borderWidth: 1,
         borderColor: "#e9e9e9",
      },
      title: "",
      tooltip: {
         formatter: function() {
            return '<b>'+ this.point.name +'</b>';
         }
      },
      plotOptions: {
         pie: {
            dataLabels: {
               enabled: true,
               color: Highcharts.theme.textColor || '#000000',
               connectorColor: Highcharts.theme.textColor || '#000000',
               formatter: function() {
                  return Math.round(this.percentage) +' %';
               }
            }
         }
      },
       series: [{
         type: 'pie',
         data: [
            {name:'Insertions', color:"#41BB1C", y: dt["ins"]},
            {name:'Deletions', color:"#ED211B", y: dt["del"]},
         ]
      }]
    });
  };
  
  // Make most active developers bar chart and table
  this.renderDevelopers = function(dt) {
    var colors = ["#A65E9D", "#DD1010", "#769DD1", "#F9F05E", "#D0D804"];
    var html = [];
    var chartData = [];
    
    // Prepare data
    for (var i in dt.developers) {
      var name = dt.developers[i][0],
          stats = dt.developers[i][1],
          s = '<tr>';
          
      s += '<td><div class="color_sample" style="background:'+ colors[i] +'"></div></td>';
      s += '<td>'+ name +'</td>';
      s += '<td>'+ stats.commits +'</td><td>'+ stats.ins +'</td><td>'+ stats.del +'</td>';
      s += '</tr>';
      
      html.push(s);
      chartData.push({"name": name, "color": colors[i], "y": stats.commits});
    }
    
    $("#developers_table").html(html.join('\n'));
    
    
    var chart = new Highcharts.Chart({
      chart: {
         renderTo: 'developers_chart',
         plotBackgroundColor: null,
         plotBorderWidth: null,
         plotShadow: true,
         borderWidth: 1,
         borderColor: "#e9e9e9",
         defaultSeriesType: 'column',
      },
      title: "",
      legend: {
        enabled: false
      },
      xAxis: {
        minorGridLineWidth: 0,
        gridLineWidth: 0,
        labels: {
          enabled: false
        }
      },
      yAxis: {
        title: {
          text: "# commits",
        }
      },
      tooltip: {
         formatter: function() {
            return '<b>' + this.point.name + '</b>';
         }
      },
      plotOptions: {
         column: {
            dataLabels: {
               enabled: true,
               color: Highcharts.theme.textColor || '#000000',
               connectorColor: Highcharts.theme.textColor || '#000000',
               formatter: function() {
                  return this.y;
               }
            },
            pointPadding: 0.01
         }
      },
       series: [{
         data: chartData
      }]
    });
  };
  
  // Fill in most changed files tables
  this.renderFiles = function(dt) {
    for (var id in dt) {
      var series = dt[id];
      var html = [];
      
      for (var i in series) {
        var cnt = '<span class="nice_num">'+ (parseInt(i) + 1) +'</span>';
        var file = series[i][0];
        var changed = series[i][1];
        html.push('<tr><td>'+ cnt +'</td><td class="shorten">'+ file +'</td><td>'+ changed +'</td></tr>');
      }
      
      $("#"+ id +"_files_table").html(html.join("\n"));
      $(".shorten").shorten({width:250});
    }
  };
}


var loader = function(){ (new Summary()).initialize(); };

